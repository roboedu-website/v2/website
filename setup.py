import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="roboedu-website-colourdelete",
    version="0.1.0",
    author="Ken Shibata et al",
    author_email="colourdelete@example.com",
    description="RoboEDU Website",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/roboedu-website/website",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPL License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)