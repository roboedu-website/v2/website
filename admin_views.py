from flask import url_for
from flask_admin.contrib.sqla import ModelView as SqlAModelView
from flask_login import current_user
from jinja2 import Markup

import database


class SqlAView(SqlAModelView):
  def is_accessible(self):
    return current_user.is_authenticated and 'admin' in current_user.group.split(' ')


class UserView(SqlAView):
  column_searchable_list = (
    database.User.username,
    database.User.user_uid,
    database.User.hash,
    database.User.group,
    database.User.first_name,
    database.User.last_name,
    database.User.age,
    database.User.notes,
  )
  column_labels = dict(
    username = 'User Name',
    user_uid = 'User UID',
    hash = 'Hash',
    group = 'Group',
    first_name = 'First Name',
    last_name = 'Last Name',
    age = 'Age',
    notes = 'Notes',
  )
  column_list = (
    database.User.username,
    database.User.user_uid,
    database.User.hash,
    database.User.group,
    database.User.first_name,
    database.User.last_name,
    database.User.age,
    database.User.notes,
  )


class CourseMaterialView(SqlAView):
  column_searchable_list = (
    database.CourseMaterial.course_uid,
    database.CourseMaterial.level,
    database.CourseMaterial.i,
    database.CourseMaterial.j,
    database.CourseMaterial.data_type,
    database.CourseMaterial.data,
  )
  column_labels = dict(
    course_uid = 'Course UID',
    level = 'Level',
    i = 'Page',
    j = 'Subpage',
    data_type = 'Data Type',
    data = 'Data',
  )
  column_list = (
    database.CourseMaterial.course_uid,
    database.CourseMaterial.level,
    database.CourseMaterial.i,
    database.CourseMaterial.j,
    database.CourseMaterial.data_type,
    database.CourseMaterial.data,
  )


class CourseView(SqlAView):
  pass


class ActionView(SqlAView):
  can_create = False
  can_edit = False
  can_delete = True
  column_searchable_list = (
    database.Action.action_time,
    database.Action.action_uid,
    database.Action.user_uid,
    database.Action.action_name,
    database.Action.action_status,
    database.Action.action_desc)
  column_labels = dict(
    action_time = 'Action Time',
    action_uid = 'Action UID',
    user_uid = 'User UID',
    action_name = 'Action Name',
    action_status = 'Action Status',
    action_desc = 'Action Description',)
  # https://flask-admin.readthedocs.io/en/latest/api/mod_model/#flask_admin.model.BaseModelView.column_formatters
  column_formatters = dict(
    action_uid = lambda v, c, m, p:
    Markup(f"<a href=\"{url_for('admin_action', action_uid = m.action_uid, prev = '/admin/action')}\">{m.action_uid}</a>"))
  column_list = (
    database.Action.action_time,
    database.Action.action_uid,
    database.Action.user_uid,
    database.Action.action_name,
    database.Action.action_status,
    database.Action.action_desc)


class IndexView(SqlAView):
  pass
