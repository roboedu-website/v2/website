from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, IntegerField, SelectField
from wtforms.validators import DataRequired


class SigninForm(FlaskForm):
  username = StringField(validators = [DataRequired()])
  password = PasswordField(validators = [DataRequired()])


class SignupForm(FlaskForm):
  username = StringField(validators = [DataRequired()])
  password = PasswordField(validators = [DataRequired()])
  group = StringField(validators = [DataRequired()])
  first_name = StringField(validators = [DataRequired()])
  last_name = StringField(validators = [DataRequired()])
  age = IntegerField(validators = [DataRequired()])
  notes = StringField(validators = [DataRequired()])


class AdminUserNewForm(FlaskForm):
  username = StringField(validators = [DataRequired()])
  password = PasswordField(validators = [DataRequired()])
  group = StringField(validators = [DataRequired()])
  first_name = StringField(validators = [DataRequired()])
  last_name = StringField(validators = [DataRequired()])
  age = IntegerField(validators = [DataRequired()])
  notes = StringField(validators = [DataRequired()])


class AdminUserChooseForm(FlaskForm):
  user_uid = SelectField(validators = [DataRequired()])


class AdminUserEditForm(FlaskForm):
  username = StringField(validators = [DataRequired()])
  password = PasswordField(validators = [DataRequired()])
  group = StringField(validators = [DataRequired()])
  first_name = StringField(validators = [DataRequired()])
  last_name = StringField(validators = [DataRequired()])
  age = IntegerField(validators = [DataRequired()])
  notes = StringField(validators = [DataRequired()])


class AdminCourseNewForm(FlaskForm):
  name = StringField(validators = [DataRequired()])
  desc = StringField(validators = [DataRequired()])
  notes = StringField(validators = [DataRequired()])


class AdminCourseChooseForm(FlaskForm):
  course_uid = SelectField(validators = [DataRequired()])


class AdminCourseEditForm(FlaskForm):
  name = StringField(validators = [DataRequired()])
  desc = StringField(validators = [DataRequired()])
  notes = StringField(validators = [DataRequired()])


class SearchForm(FlaskForm):
  query = StringField(validators = [DataRequired()])
